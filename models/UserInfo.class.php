<?php 

class UserClass {
	
    /*
    public $email = NULL;
	public $hash_pw = NULL;
	public $user_id = NULL;
    */

    public $conn = NULL;
    

    function __construct($connection) {
        $this->conn = $connection;
    }

    public function getAllUserInformation($parameter){

        $returnData = [];

        $token= $parameter['token'];

       // UPDATE `users` SET `id`=[value-1],`name`=[value-2],`email`=[value-3],`short_description`=[value-4],`img`=[value-5],`birthdate`=[value-6],`adress`=[value-7],`password`=[value-8],`permission`=[value-9] WHERE 1

        $query_info = "SELECT `users`.* FROM `users`
                        WHERE `activation_token`=:activation_token
                      ";
        $select_info_stmt =  $this->conn->prepare($query_info);
        $select_info_stmt->bindValue(':activation_token', $parameter['token'] ,PDO::PARAM_STR);
       
        $select_info_stmt->execute();
        
        if($select_info_stmt->rowCount()){

            $row = $select_info_stmt->fetch(PDO::FETCH_ASSOC);
            
            $pr = [];
            $pr['token'] = $row['activation_token'];

            $activate = $this->activateAccount($pr);
            
            $returnData = msg(1,200,$activate);
            

        }else{
            $returnData = msg(0,422,"Something went wrong, the token is unavailable or it has expired!");
        }

       

        $select_info_stmt = NULL;	

        return $returnData;

    }
	
	//Simple function to update the last sign in of a user
	public function updateProfileSectionData($parameter)
	{
        $returnData = [];

        $name= $parameter['name'];
        $short_description= $parameter['short_description'];
        $birthdate= $parameter['birthdate'];
        $adress= $parameter['adress'];
        $city= $parameter['city'];
        $country= $parameter['country'];
        $email= $parameter['email'];

       // UPDATE `users` SET `id`=[value-1],`name`=[value-2],`email`=[value-3],`short_description`=[value-4],`img`=[value-5],`birthdate`=[value-6],`adress`=[value-7],`password`=[value-8],`permission`=[value-9] WHERE 1

        $update_info = "UPDATE `users` SET 
                        `name`=:name, 
                        `short_description`=:short_description,
                        `birthdate`=:birthdate, 
                        `adress`=:adress,
                        `city`=:city,
                        `country`=:country
                        WHERE `email`=:email
                       ";
        $update_info_stmt =  $this->conn->prepare($update_info);
        $update_info_stmt->bindValue(':name', htmlspecialchars(strip_tags($name)),PDO::PARAM_STR);
        $update_info_stmt->bindValue(':short_description', $short_description,PDO::PARAM_STR);
        $update_info_stmt->bindValue(':birthdate', $birthdate,PDO::PARAM_STR);
        $update_info_stmt->bindValue(':adress', $adress,PDO::PARAM_STR);
        $update_info_stmt->bindValue(':city', $city,PDO::PARAM_STR);
        $update_info_stmt->bindValue(':country', $country,PDO::PARAM_STR);
        $update_info_stmt->bindValue(':email', $email,PDO::PARAM_STR);

        $update_info_stmt->execute();
      
        $returnData = msg(1, $update_info_stmt ,'Fields updated successfully.');

        $update_info_stmt = NULL;	

        return $returnData;
        
	}

    public function activateAccount($parameter)
	{
        $returnData = [];

        $token= $parameter['token'];

       // UPDATE `users` SET `id`=[value-1],`name`=[value-2],`email`=[value-3],`short_description`=[value-4],`img`=[value-5],`birthdate`=[value-6],`adress`=[value-7],`password`=[value-8],`permission`=[value-9] WHERE 1

        $update_info = "UPDATE `users` SET 
                        `active` = 1
                        WHERE `activation_token`=:token
                       ";
        $update_info_stmt =  $this->conn->prepare($update_info);
        $update_info_stmt->bindValue(':token', $token, PDO::PARAM_STR);
        $update_info_stmt->execute();
      
        $returnData = msg(1, $update_info_stmt ,'Your account is now activated.');

        $update_info_stmt = NULL;	

        return $returnData;
        
	}

    //Simple function to update the last sign in of a user
	public function updateProfilePicture($parameter)
	{
        $returnData = [];

        $imgstring= $parameter['imgstring'];
        $email= $parameter['email'];

       // UPDATE `users` SET `id`=[value-1],`name`=[value-2],`email`=[value-3],`short_description`=[value-4],`img`=[value-5],`birthdate`=[value-6],`adress`=[value-7],`password`=[value-8],`permission`=[value-9] WHERE 1

        $update_info = "UPDATE `users` SET 
                        `img`=:imgstring
                        WHERE `email`=:email
                       ";
        $update_info_stmt =  $this->conn->prepare($update_info);
        
        $update_info_stmt->bindValue(':imgstring', $imgstring ,PDO::PARAM_STR);
        $update_info_stmt->bindValue(':email', $email, PDO::PARAM_STR);

        $update_info_stmt->execute();
      
        $returnData = msg(1, [$update_info_stmt, $parameter['imgstring']] ,'Profile Image updated successfully.');

        $update_info_stmt = NULL;	

        return $returnData;
        
	}

    //Simple function to update the last sign in of a user
	public function updateProfileCoverImg($parameter)
	{
        $returnData = [];

        $imgstring= $parameter['imgstring'];
        $email= $parameter['email'];

       // UPDATE `users` SET `id`=[value-1],`name`=[value-2],`email`=[value-3],`short_description`=[value-4],`img`=[value-5],`birthdate`=[value-6],`adress`=[value-7],`password`=[value-8],`permission`=[value-9] WHERE 1

        $update_info = "UPDATE `users` SET 
                        `cover_img`=:imgstring
                        WHERE `email`=:email
                       ";
        $update_info_stmt =  $this->conn->prepare($update_info);
        
        $update_info_stmt->bindValue(':imgstring', $imgstring ,PDO::PARAM_STR);
        $update_info_stmt->bindValue(':email', $email, PDO::PARAM_STR);

        $update_info_stmt->execute();
      
        $returnData = msg(1, [$update_info_stmt, $parameter['imgstring']] ,'Cover Image updated successfully.');

        $update_info_stmt = NULL;	

        return $returnData;
        
	}

    public function moveResizeAndUnlikOldProfilePicture($parameters){

        // TRANSPORT THE NEW IMAGE IN THE DESTINATION 
        $fl = base64_to_file($parameters['base_64_string'], $parameters['path'].$parameters['destination_for_the_file']);
        //sleep);
        // RESIZE THE IMAGE 
        resize_crop_image(400, 400, $parameters['path'].$parameters['destination_for_the_file'], $parameters['path'].$parameters['destination_for_the_file'], $quality = 70);

        // DELETE THE OLD IMAGE FROM DISK
        if($parameters['old_image']  != ''){
            unlink( $parameters['path'].$parameters['old_image'] );
        }
        return true;
    }

    public function moveResizeAndUnlikOldCoverPicture($parameters){

        // TRANSPORT THE NEW IMAGE IN THE DESTINATION 
        $fl = base64_to_file($parameters['base_64_string'], $parameters['path'].$parameters['destination_for_the_file']);
        //sleep);
        // RESIZE THE IMAGE 
        resize_crop_image(1000, 500, $parameters['path'].$parameters['destination_for_the_file'], $parameters['path'].$parameters['destination_for_the_file'], $quality = 70);

        // DELETE THE OLD IMAGE FROM DISK
        if($parameters['old_image']  != ''){
            unlink( $parameters['path'].$parameters['old_image'] );
        }
        return true;
    }

}

?>