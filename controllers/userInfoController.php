<?php
require '../classes/configs.php';

$db_connection = new Database();
$conn = $db_connection->dbConnection();

$data = json_decode(file_get_contents("php://input"));
$returnData = ['data-returned'];
// IF REQUEST METHOD IS NOT EQUAL TO POST
if($_SERVER["REQUEST_METHOD"] != "POST"){
    $returnData = msg(0,404,'Page Not Found!');
}
// CHECKING EMPTY FIELDS
else{

    $UserClass = new UserClass($conn);

    switch ($data->action){
       
        case 'activate_user':

            $returnData = $data->parameter->tk;
            
            $parameter = [];
            $parameter["token"] = $data->parameter->tk;

            $updateProfileSectionData = $UserClass->getAllUserInformation($parameter);
                    
            $returnData = $updateProfileSectionData; 

           

        break;    
        
        // in case the user updates the information
        case 'upd_info':
       
                if(
                empty($data->parameter->{'name'}) ||
                empty($data->parameter->{'email'}) ||
                empty($data->parameter->{'short_description'}) ||
                empty($data->parameter->{'adress'}) ||
                empty($data->parameter->{'birthdate'}) 
                ){
            
                    $fields = ['fields' => [
                            'name',    
                            'email',
                            'short_description', 
                            'adress', 
                            'birthdate'
                    ]];
                
                    $returnData = msg(0,422,'Please Fill in all Required Fields!',$fields);
                    //$returnData = $data->parameter;)

                }  else {

                    $parameter = [];

                    $parameter['name'] = $data->parameter->{'name'};
                    $parameter['short_description'] = $data->parameter->{'short_description'};
                    $parameter['birthdate'] = $data->parameter->{'birthdate'};
                    $parameter['adress'] = $data->parameter->{'adress'};
                    $parameter['city'] = $data->parameter->{'city'};
                    $parameter['country'] = $data->parameter->{'country'};
                    $parameter['email'] = $data->parameter->{'email'};
                
                    //file_put_contents("test.txt", $dataRaw->file );
                
                    $updateProfileSectionData = $UserClass->updateProfileSectionData($parameter);
                    
                    $returnData = $updateProfileSectionData; 

                }    

        break;

        // in case the user updates the image of his profile
        case 'upd_profile_pic':
            
            $returnData = $data; 

            $destinationFile = "uploads/profileimg/".GUID()."-".GUID().".jpg";

            $parametersImageUpload = [];

            $parametersImageUpload['base_64_string'] = $data->parameter[1];
            $parametersImageUpload['destination_for_the_file'] = $destinationFile;
            $parametersImageUpload['old_image'] = $data->parameter[2];
            $parametersImageUpload['path'] = $configs['BASE_PATH'];

            $updateProfileImage =  $UserClass->moveResizeAndUnlikOldProfilePicture( $parametersImageUpload ); 
            
            /*    
            // TRANSPORT THE NEW IMAGE IN THE DESTINATION 
            $fl = base64_to_file($data->parameter[1], $destinationFile);
            //sleep);
            // RESIZE THE IMAGE 
            resize_crop_image(400, 400, $destinationFile, $destinationFile, $quality = 70);

            // DELETE THE OLD IMAGE FROM DISK
            if($data->parameter[2] != ''){
                unlink($data->parameter[2]);
            }
            */

            $parameter = [];
            $parameter['email'] = $data->parameter[0];
            $parameter['imgstring'] =  $destinationFile;
    
            $updateProfileImage =  $UserClass->updateProfilePicture( $parameter );

            $returnData = $updateProfileImage; 
            //file_put_contents("test.txt", json_encode($_FILES) );

        break;

        // in case the user updates the image of his profile
        case 'upd_profile_cover_img':
            
            $returnData = $data; 

            $destinationFile = "uploads/coverimg/".GUID()."-".GUID().".jpg";

            $parametersImageUpload = [];

            $parametersImageUpload['base_64_string'] = $data->parameter[1];
            $parametersImageUpload['destination_for_the_file'] = $destinationFile;
            $parametersImageUpload['old_image'] = $data->parameter[2];
            $parametersImageUpload['path'] = $configs['BASE_PATH'];

            $updateProfileImage =  $UserClass->moveResizeAndUnlikOldCoverPicture( $parametersImageUpload ); 
			$updateProfileImage['upd'] = $parametersImageUpload;
		  

            $parameter = [];
            $parameter['email'] = $data->parameter[0];
            $parameter['imgstring'] =  $destinationFile;
    
            $updateProfileImage =  $UserClass->updateProfileCoverImg( $parameter );

            $returnData = $updateProfileImage; 
            //file_put_contents("test.txt", json_encode($_FILES) );
            
        break;
    }    
}

echo json_encode($returnData);