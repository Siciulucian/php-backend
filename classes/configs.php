<?php 
$configs = [];
$configs['BASE_PATH']  = 'C:\xampp\htdocs\php-login-registration-api\/';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require '../classes/Database.php';
require '../classes/JwtHandler.php';
require '../models/UserInfo.class.php';
require '../libs/functions_static.php';
?>