-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 27, 2021 at 10:07 AM
-- Server version: 5.7.35
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bluelogi_reactbp`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(1200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover_img` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `city` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(1) DEFAULT NULL,
  `activation_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `short_description`, `img`, `cover_img`, `birthdate`, `city`, `country`, `adress`, `password`, `active`, `activation_token`, `permission`) VALUES
(8, 'Valentin Lucian Mehedinteanu', 'lucian@lucian.com', 'Experienced Mobile Application Developer with a demonstrated history of working in the internet industry. Skilled in PhoneGap, PHP, Apache Cordova, jQuery, and Web Design, Web Development. React quick learner.', 'uploads/profileimg/15DD3B68-6181-4A61-BCB0-C58A87CE2733-3F09B785-60D9-4E38-B883-45CCA7EDF821.jpg', 'uploads/coverimg/A862F435-3CE9-4CB1-B721-CC6A016B1AB1-5A926DBD-694A-4D1D-AC32-C09DB7FBE78B.jpg', '1990-06-19', 'Craiova', 'Romania', 'Trandafirului street no. 68', '$2y$10$29I3qC3oPQ/UMA3jPu7Ew.XQ5.uUc38.TRd.kfQIWJlst1FpZybrm', 1, '990FC902-A993-4268-953E-5BCB6725048E-B744550D-88CB-4B9A-UUUU-5E8F5F3E6F97', 1),
(9, 'Adrian Bogdan Agacitei ', 'adrian@adrian.ro', 'Experienced Mobile Application Developer with a demonstrated history of working in the internet industry. Skilled in React N, PHP, Apache Cordova, jQuery, and Web Design, Web Development.', 'uploads/profileimg/F1A90227-4BAF-4264-9C3C-EB15BEB90495-137F6561-28D1-42B4-A1E5-619B51DD13A6.jpg', 'uploads/coverimg/B7D6C13B-6142-4E55-81F3-A0AC3F1E0CCC-82519D00-4DFF-4EE0-ABFF-8812215C419F.jpg', '2021-08-16', 'Craiova', 'Romania', 'Trandafirului Street no. 68', '$2y$10$LeBE02z8uEuOLcD9p94vPOMRggAetO0WZY/RvEhTfVfE73bJ5ggS.', 1, '990FC902-A993-4268-953E-5BCB6725048E-B744550D-7788-4B9A-BBCD-5E8F5F3E6F97', 2),
(12, 'Lucian Valentin Mehedinteanu', 'lucianmehedinteanu90@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$XoVmELKSMroCRpw2ZdxmnOsVyNEEUA48mPg4mtk808ffFKG.NHzNG', 0, '990FC902-A993-4268-953E-5BCB6725048E-B744550D-88CB-4B9A-BBCD-5E8F5F3E6F97', 2),
(14, 'Lucian Mehedinteanu', 'lucian_mehedinteanu@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$I0aSHIe.RVl38S6kbwMXhelCmpHXQqDbdYKIZyJQ/8L7HPzSmlu2i', 0, '66FC83FB-6761-4CCF-8FF4-0C598C023D27-EFEEF6E9-8443-4FCF-86CE-B1575BB6CCEB', 2),
(15, 'Marco Polo', 'marcopolo@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$FlVGcOyWR4UUWb9.Fc91kOqWm/dlwMOjULEXsUdLIuzGnOX.V59Ze', 0, '415441FA-0118-432F-BC2D-DBE2BE1A639B-ECFDACD4-D53A-4E3E-AFB1-7C3858FA9D7A', 2),
(16, 'lucian.mehedinteanu@bluelogic.ro', 'lucian.mehedinteanu@bluelogic.ro', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$LYTeZbpv5rrWnACN/JpVu.kmdreMEa/ufLm94NtCAcqEIoPQOMLxO', 1, '9D266B4F-7B49-4CF7-8C07-3BEE1A578270-1F59F953-F782-49D2-9A64-2D98DA5435EE', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
